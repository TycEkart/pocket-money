package com.abnamro.pocketmoney.models

import android.support.test.runner.AndroidJUnit4
import com.abnamro.pocketmoney.canvas.Vector2
import org.jetbrains.anko.collections.forEachByIndex
import org.jetbrains.anko.custom.async
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CoinTest {
    @Test
    fun getListOfCoinsForAmountTest() {

        var list = Coin.getListOfCoinsForAmount(6)
        assertEquals(6, list.sumBy { it.coinEnum.centValue })

        list = Coin.getListOfCoinsForAmount(142)
        assertEquals(142, list.sumBy { it.coinEnum.centValue })

        list = Coin.getListOfCoinsForAmount(4027)
        assertEquals(4027, list.sumBy { it.coinEnum.centValue })

        //Check if deterministic:
        list = Coin.getListOfCoinsForAmount(163)
        var samelist = Coin.getListOfCoinsForAmount(163)
        assertEquals(list[0].coinEnum, samelist[0].coinEnum)
        assertEquals(list[1].coinEnum, samelist[1].coinEnum)
        assertEquals(list[2].coinEnum, samelist[2].coinEnum)

    }
}