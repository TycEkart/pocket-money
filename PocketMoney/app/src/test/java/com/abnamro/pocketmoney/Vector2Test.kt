package com.abnamro.pocketmoney

import com.abnamro.pocketmoney.canvas.Vector2
import com.abnamro.pocketmoney.canvas.minus
import junit.framework.Assert
import org.junit.Test

class Vector2Test {

    @Test
    fun minTest() {


        val a = Vector2(0f, 0f)
        val b = Vector2(3f, 2f)


        val c = Vector2(-3f, -2f)

        Assert.assertEquals(c, a.min(b))
        Assert.assertEquals(c, a - b)

    }

    @Test
    fun magnitudeTest() {
        val a = Vector2(5f, 12f)
        Assert.assertEquals(13f, a.magnitude())
    }


    @Test
    fun distance() {
        val a = Vector2(0f, 0f)
        val b = Vector2(5f, 12f)

        Assert.assertEquals(13f, a.distance(b))

    }

}