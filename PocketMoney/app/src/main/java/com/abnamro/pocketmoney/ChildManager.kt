package com.abnamro.pocketmoney

import android.content.Context
import android.util.Log
import com.abnamro.pocketmoney.models.ChildUser
import com.google.firebase.database.*


/**
 * Created by Thijs Eckhart on 18-4-2018.
 */
object ChildManager {
    var loginState = LoginState.UNKNOWN;
    var database = FirebaseDatabase.getInstance()

    private val TAG = ChildManager::class.java.simpleName
    var user: ChildUser = ChildUser()

    fun login(userId: String, password: String, context: Context, completed: (ChildUser) -> Unit, onFailed: (LoginState) -> Unit) {
        val myRef = database.getReference("users").child(userId)

        myRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.i(TAG, "onCancelled: $p0")
                loginState = LoginState.LOGGED_OUT
                onFailed(loginState)
                saveOrClear(userId, password, context)
            }

            override fun onDataChange(incomingData: DataSnapshot) {
                Log.i(TAG, "onDataChange: $incomingData")
                val user = incomingData.getValue(ChildUser::class.java)
                if (user != null) {
                    if (password == user.password) {
                        loginState = LoginState.LOGGED_IN
                        this@ChildManager.user = user
                        completed(user)
                        saveOrClear(userId, password, context)
                    } else {
                        loginState = LoginState.WRONG_PASSWORD
                        onFailed(loginState)
                        saveOrClear(userId, password, context)
                    }
                } else {
                    loginState = LoginState.USER_UNKNOWN
                    onFailed(loginState)
                    saveOrClear(userId, password, context)
                }
            }
        })

    }

    private fun saveOrClear(userId: String, password: String, context: Context) {
        val sharedPreferences = context.getSharedPreferences("PocketMoney", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        if (loginState == LoginState.LOGGED_IN) {
            editor.putString("userId", userId)
            editor.putString("password", password)
            editor.commit()
        } else {
            editor.remove("userId")
            editor.remove("password")
            editor.commit()
        }
    }

    fun tryLogin(context: Context, onSuccess: (ChildUser) -> Unit, onFailed: (LoginState) -> Unit) {
        val sharedPreferences = context.getSharedPreferences("PocketMoney", Context.MODE_PRIVATE)
        val userId = sharedPreferences.getString("userId", null)
        val password = sharedPreferences.getString("password", null)
        if (userId != null && password != null) {
            login(userId, password, context, onSuccess, onFailed)
        } else {
            onFailed(LoginState.LOGGED_OUT)
        }
    }

    fun logout(context: Context) {
        loginState = LoginState.LOGGED_OUT
        saveOrClear("", "", context)
    }


    enum class LoginState {
        LOGGED_IN, LOGGED_OUT, WRONG_PASSWORD, UNKNOWN, USER_UNKNOWN
    }


    private fun getRef(): DatabaseReference? {
        val user = this.user
        return if (user.userId != -1) {
            database.getReference("users").child(user.userId.toString())
        } else {
            Log.w(ChildManager::class.java.simpleName, "getRef(): userId == -1")
            null
        }
    }

    fun updateDB() {
        val ref = getRef()
        if (ref != null) {
            ref.setValue(user)
        } else {
            Log.w(ChildManager::class.java.simpleName, "updateDB(): userId == -1")
        }
    }

    private var registeredListener: (() -> Unit)? = null
    private var eventListener = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {}

        override fun onDataChange(incomingData: DataSnapshot) {
            val incomingUser = incomingData.getValue(ChildUser::class.java)
            if (user.userId == incomingUser?.userId) {
                user = incomingUser
            } else {
                Log.w(ChildManager::class.java.simpleName, "onDataChange(): incoming $incomingUser vs $user")
            }
            registeredListener?.invoke()
        }
    }


    fun registerOnUserChangeListener(listener: (() -> Unit)) {
        if (this.registeredListener == null) {
            this.registeredListener = listener
            getRef()?.addValueEventListener(eventListener)
        } else {
            this.registeredListener = listener
        }
    }

    fun unregister() {
        this.registeredListener = null
        getRef()?.removeEventListener(eventListener)
    }
}