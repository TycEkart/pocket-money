package com.abnamro.pocketmoney.canvas

data class Vector2(val x: Float = 0f, val y: Float = 0f) {

    fun min(a: Vector2) = Vector2(this.x - a.x, this.y - a.y)
    fun add(a: Vector2) = Vector2(this.x + a.x, this.y + a.y)

    fun distance(a: Vector2): Float = (a - this).magnitude()

    fun magnitude() = Math.sqrt((x * x + y * y).toDouble()).toFloat()
    fun divide(factor: Float): Vector2 = Vector2(x / factor, y / factor)
    fun multiply(factor: Float): Vector2 = Vector2(x * factor, y * factor)
}

operator fun Vector2.minus(a: Vector2) = min(a)
operator fun Vector2.plus(a: Vector2) = add(a)
