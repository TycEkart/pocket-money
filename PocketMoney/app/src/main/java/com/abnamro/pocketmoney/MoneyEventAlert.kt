package com.abnamro.pocketmoney

import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import com.abnamro.pocketmoney.models.Event
import kotlinx.android.synthetic.main.alert_money_event.view.*
import org.jetbrains.anko.alert

class MoneyEventAlert(private val event: Event, private val continueListener: (event: Event) -> Unit) {
    private var dialog: DialogInterface? = null
    private lateinit var customViewCustom: View
    fun show(context: Context) {
        dialog = context.alert {
            titleResource = R.string.money_event_alert_title
            val view = LayoutInflater.from(context).inflate(R.layout.alert_money_event, null, false)
            customView = view
            customViewCustom = view
            init(view)
        }.show()

        val d = dialog
        if (d is AlertDialog) {
            d.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        }
    }

    fun dismiss() {
        dialog?.dismiss()
    }

    fun init(view: View) {
        view.continueButton.setOnClickListener {
            view.continueButton.isEnabled = false
            continueListener.invoke(event)
            dismiss()
        }

        view.eventTextView.text = event.text + "\nCentjes: " + event.amount
    }
}