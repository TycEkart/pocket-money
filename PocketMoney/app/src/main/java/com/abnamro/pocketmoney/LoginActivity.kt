package com.abnamro.pocketmoney

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.content_login.*
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setSupportActionBar(toolbar)
        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        editTextPassword.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE || (event != null && (event.keyCode == KeyEvent.KEYCODE_ENTER))) {
                // do something, e.g. set your TextView here via .setText()
                val imm = v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)

                buttonLogin.requestFocus()

                return@OnEditorActionListener true
            }
            false
        })


        buttonLogin.setOnClickListener {

            editTextPassword.isEnabled = false
            editTextId.isEnabled = false
            buttonLogin.isEnabled = false
            ChildManager.login(
                    editTextId.text.toString(),
                    editTextPassword.text.toString(),
                    baseContext,
                    { startActivity(Intent(baseContext, MoneyActivity::class.java)) },
                    {
                        editTextPassword.isEnabled = true
                        editTextId.isEnabled = true
                        buttonLogin.isEnabled = true
                        Toast.makeText(baseContext, "Nope failed log $it", Toast.LENGTH_SHORT).show()
                    })
        }
    }
}

