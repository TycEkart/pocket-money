package com.abnamro.pocketmoney.canvas

import android.content.Context
import android.graphics.*
import android.view.View
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import com.abnamro.pocketmoney.ChildManager
import com.abnamro.pocketmoney.models.Coin

internal class CustomView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0,
        defStyleRes: Int = 0) : View(context, attrs, defStyle, defStyleRes), View.OnTouchListener {

    var onNoMoreCoinsListener: (() -> Unit)? = null

    val drawables = arrayListOf<CustomDrawable>(
    )

    fun addDrawables(draw: CustomDrawable) {
        drawables.add(draw)
        this.invalidate()
    }

    private var transFormationMatrix = Matrix() // transformation matrix

    private var startTime: Long = 0

    init {
        //start the animation:
        Coin.setCoins(context)
        this.startTime = System.currentTimeMillis()
        this.postInvalidate()
        this.setOnTouchListener(this)
    }

    override fun onDraw(canvas: Canvas) {
        canvas.concat(transFormationMatrix)        // call this before drawing on the canvas!!
        drawables.forEach { it.onDraw(canvas) }
    }

    var movingCoin: Coin? = null

    override fun onTouch(v: View?, event: MotionEvent): Boolean {
        val outPointerCoords = MotionEvent.PointerCoords()
        event.getPointerCoords(0, outPointerCoords)

        return when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                //Log.d("Hi", "event.action ${outPointerCoords.x}")
                for (draw in drawables) {
                    if (draw.isInBound(Vector2(outPointerCoords.x, outPointerCoords.y))) {
                        //onclickDrawable(draw)
                        if (draw::class.java == Coin::class.java) {
                            movingCoin = draw as Coin
                        }
                        return true
                    }
                }
                false
            }
            MotionEvent.ACTION_MOVE -> {
                val movingDraw = movingCoin
                if (movingDraw != null) {
                    println("mvoing" + movingDraw)
                    if (movingDraw.dragTo(Vector2(outPointerCoords.x, outPointerCoords.y))) {
                        //moved
                        invalidate()
                    }
                }
                true
            }
            MotionEvent.ACTION_UP -> {
                val movingDraw = movingCoin
                println(movingDraw.toString() + "up")
                if (movingDraw != null) {
                    println("not null")
                    if (movingDraw.isInTopRightCorner()) {
                        Log.d("test", "wtf12")
                        onclickDrawable(movingDraw)
                        true
                    } else {
                        println("outbound")
                        false
                    }
                } else {
                    println("up null")
                    false
                }
            }
            else -> false
        }
    }


    /**
     * Can be changed in an onClick. (basically we would put the onClick logic at a different place
     */
    private fun onclickDrawable(coin: Coin) {
        ChildManager.user.amount += coin.coinEnum.centValue
        ChildManager.updateDB()
        drawables.remove(coin)
        invalidate()
        if (drawables.none { it::class.java == Coin::class.java }) {
            onNoMoreCoinsListener?.invoke()
        }
    }
}

interface CustomDrawable {
    fun onDraw(canvas: Canvas)
    fun isInBound(location: Vector2): Boolean
}

