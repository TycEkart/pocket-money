package com.abnamro.pocketmoney

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import com.abnamro.pocketmoney.models.Coin
import kotlinx.android.synthetic.main.activity_money.*
import kotlinx.android.synthetic.main.activity_money.view.*
import kotlinx.android.synthetic.main.content_money.*
import java.util.*


class MoneyActivity : AppCompatActivity() {

    private var happyToggle = true //Used for the happiness animations

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_money)
        customView.onNoMoreCoinsListener = onNoMoreCoinsListener
        initActionBar()
        initUserInfo()
        piggyImage.setOnClickListener {
            val events = ChildManager.user.events.filter { it.value.type == "newmoney" }
            //ignore other events for now
            if (events.isNotEmpty()) {
                val eventPair = events.entries.sortedBy { it.value.time }.first()
                val event = eventPair.value.copy(id = eventPair.key)
                MoneyEventAlert(event) {
                    piggyImage.isEnabled = false
                    ChildManager.user.events.remove(it.id)
                    ChildManager.updateDB()
                    Coin.getListOfCoinsForAmount(it.amount).forEach {
                        customView.addDrawables(it)
                    }
                }.show(this)
            } else {
                makeHappy()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        ChildManager.registerOnUserChangeListener {
            initUserInfo()
        }
    }

    override fun onPause() {
        super.onPause()
        ChildManager.unregister()
    }


    private fun initUserInfo() {
        val user = ChildManager.user
        name.text = user.name
        amount.text = "${user.amount}"
        if (user.events.isNotEmpty()) {
            makeHappy()
            piggyImage.text = "Click Me!"
        } else {
            piggyImage.text = ""
        }
    }

    private val onNoMoreCoinsListener: (() -> Unit) = {
        piggyImage.isEnabled = true
    }


    private fun initActionBar() {
        setSupportActionBar(toolbar)
        toolbar.toolbar_title.setOnClickListener {
            AlertDialog.Builder(this)
                    .setPositiveButton(getString(R.string.text_ok)) { _, _ ->
                        ChildManager.logout(baseContext)
                        startActivity(Intent(baseContext, LoginActivity::class.java))
                    }
                    .setNegativeButton("Cancel") { _, _ -> }
                    .setTitle(getString(R.string.text_logout_verification))
                    .show()
        }
    }

    private fun makeHappy() {
        //Change image to 'happy' or 'jump':
        if (happyToggle)
            piggyImage.setBackgroundResource(R.drawable.happy1)
        else
            piggyImage.setBackgroundResource(R.drawable.jump1)
        happyToggle = !happyToggle

        //After 1 second, change it back
        Timer().schedule(object : TimerTask() {
            override fun run() {
                piggyImage.setBackgroundResource(R.drawable.idle_loop)
            }
        }, 1000)
    }
}
