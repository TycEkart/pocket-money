package com.abnamro.pocketmoney.models

data class Event(val id: String = "",
                 val amount: Int = 0,
                 val text: String = "",
                 val time: Long = 0L,
                 val type: String = "Unknown Event")