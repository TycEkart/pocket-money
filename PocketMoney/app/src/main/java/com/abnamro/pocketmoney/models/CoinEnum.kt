package com.abnamro.pocketmoney.models

import com.abnamro.pocketmoney.R

/**
 * specs from: https://www.fleur-de-coin.com/eurocoins/specifications
 */
enum class CoinEnum(val centValue: Int, val radius: Int, val drawableRes: Int) {
    CENT_1(1, 1625 / 2, R.drawable.cent_001),
    CENT_2(2, 1875 / 2, R.drawable.cent_002),
    CENT_5(5, 2125 / 2, R.drawable.cent_005),
    CENT_10(10, 1975 / 2, R.drawable.cent_010),
    CENT_20(20, 2225 / 2, R.drawable.cent_020),
    CENT_50(50, 2425 / 2, R.drawable.cent_050),
    EURO_1(100, 2325 / 2, R.drawable.cent_100),
    EURO_2(200, 2575 / 2, R.drawable.cent_200)
}