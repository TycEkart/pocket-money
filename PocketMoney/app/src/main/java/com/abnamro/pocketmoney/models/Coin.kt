package com.abnamro.pocketmoney.models

import android.content.Context
import android.graphics.*
import com.abnamro.pocketmoney.canvas.CustomDrawable
import com.abnamro.pocketmoney.canvas.Vector2
import java.util.*
import kotlin.collections.ArrayList


data class Coin(val coinEnum: CoinEnum, var position: Vector2 = Vector2()) : CustomDrawable {
    companion object {
        const val PERCENTAGE_MULTIPLIER = 0.01f
        const val COIN_SCALE = 0.005f

        val sortedList = CoinEnum.values().sortedBy { -it.centValue }
        /**
         * Selects a semi-random set of coins to cover a euro amount
         * Semi-deterministic
         * Favours larger coins
         * @param totalCents the total amount, in Coins
         */
        fun getListOfCoinsForAmount(totalCents: Int): List<Coin> {
            var amountRemaining = totalCents
            //Seed random to make deterministic:
            val rand = Random(totalCents.toLong())
            val coinEnums = ArrayList<CoinEnum>()
            //Loop until full amount is covered:
            while (amountRemaining > 0) {
                for (coin in sortedList) {
                    val randomNum = rand.nextInt(10)
                    //Semi-randomly select a coin:
                    if (coin.centValue <= amountRemaining && randomNum > 4) {
                        coinEnums.add(coin)
                        amountRemaining -= coin.centValue
                        break
                    }
                }
            }
            return randomSpawnCoin(coinEnums)
        }

        private fun randomSpawnCoin(coinEnums: ArrayList<CoinEnum>): List<Coin> {
            //calculate square field
            val mapSize = coinEnums.size * 4 //more spots
            val sq = Math.sqrt(mapSize.toDouble())
            val size = Math.floor(sq + 0.5).toInt()

            val coordinatesStack = Stack<Vector2>()
            for (x in 1 until size) {
                (1 until size).mapTo(coordinatesStack) { y -> Vector2(x.toFloat(), y.toFloat()) }
            }


            //random spawns of coins
            Collections.shuffle(coordinatesStack)

            val returnList = ArrayList<Coin>()
            coinEnums.forEach {
                val coordinate = coordinatesStack.pop().divide(size.toFloat()).multiply(100f) //percentage
                returnList.add(Coin(it, coordinate))
            }
            return returnList
        }

        val bitmapCoinMap: HashMap<CoinEnum, Bitmap> = hashMapOf()

        fun setCoins(context: Context) {
            val res = context.resources
            CoinEnum.values().forEach {
                val bm = BitmapFactory.decodeResource(res, it.drawableRes);
                bitmapCoinMap[it] = bm
            }
        }
    }


    private var pxRadius = -1f
    private var pxPosition = Vector2()

    override fun isInBound(location: Vector2): Boolean {
        println("is in bound $pxPosition")
        //Log.e("POS", "$pxPosition")
        //Log.e("LOC", "$location")
        val distance = pxPosition.distance(location)
        // Log.e("D", "$distance")
        //Log.e("r", "$pxRadius")
        println("distanace $distance < $pxRadius")
        return distance < pxRadius
    }

    private var paint = Paint().also { it.color = Color.BLACK }

    var canvasWidth = 0
    var canvasHeight = 0
    override fun onDraw(canvas: Canvas) {
        if (canvas.width != canvasWidth || canvas.height != canvasHeight) {
            canvasWidth = canvas.width
            canvasHeight = canvas.height
            pxRadius = canvasWidth * PERCENTAGE_MULTIPLIER * coinEnum.radius * COIN_SCALE
            pxPosition = Vector2(canvasWidth * PERCENTAGE_MULTIPLIER * position.x, canvasHeight * PERCENTAGE_MULTIPLIER * position.y)
        }
        val left = (pxPosition.x - pxRadius).toInt()
        val right = (left + pxRadius * 2f).toInt()
        val top = (pxPosition.y - pxRadius).toInt()
        val bot = (top + pxRadius * 2f).toInt()

        val bitmap = bitmapCoinMap[coinEnum]

        canvas.drawBitmap(bitmap, null, Rect(left, top, right, bot), paint)
    }

    fun dragTo(vector2: Vector2): Boolean {
        return if (position != vector2) {
            position = Vector2(vector2.x / canvasWidth * 100, vector2.y / canvasHeight * 100)

            pxPosition = Vector2(canvasWidth * PERCENTAGE_MULTIPLIER * position.x, canvasHeight * PERCENTAGE_MULTIPLIER * position.y)
            true
        } else {
            false
        }
    }

    fun isInTopRightCorner(): Boolean {
        return this.position.x > 90 && this.position.y < 10
    }
}

