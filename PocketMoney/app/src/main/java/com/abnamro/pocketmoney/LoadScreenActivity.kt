package com.abnamro.pocketmoney

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class LoadScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()

        ChildManager.tryLogin(baseContext,
                {
                    startActivity(Intent(baseContext, MoneyActivity::class.java))
                },
                {
                    startActivity(Intent(baseContext, LoginActivity::class.java))
                })
    }
}
