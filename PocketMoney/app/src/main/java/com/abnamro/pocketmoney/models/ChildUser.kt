package com.abnamro.pocketmoney.models

/**
 * Created by Thijs Eckhart on 3-5-2018.
 */
data class ChildUser(val userId: Int = -1,
                     var amount: Int = 0,
                     val password: String = "",
                     val name: String = "",
                     val events: HashMap<String, Event> = HashMap())

