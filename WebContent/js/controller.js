angular.module('pocketMoneyApp').controller('AccountCtrl', function($scope, AccountService, $firebaseObject, $routeParams) {

	$scope.stats = [];
	
	
	
	$scope.showGiveMoneyForm = false;
	
	$scope.accountID = -1; //init
	$scope.accountService = AccountService
	
	//TODO use routeParams to get accountID
	
	
	$scope.login = function() {
		$scope.accountID = $scope.accountIdInput
		AccountService.login($scope.accountID);
	}
	
	//Used to initialise the form to give new pocket money
	//Sets value to 0
	$scope.initGiveMoneyForm = function() {
		$scope.giveAmount = 0;
		$scope.showGiveMoneyForm = true;
	}
	
	//Submit a pocketmoney gift to the backend
	$scope.submitGiveMoney = function() {
		if (isNaN($scope.giveAmount)) {
			console.log("Not a number!");
			return;
		}
		
		AccountService.giveMoney($scope.giveAmount);
		
		//TODO wait for response
		$scope.showGiveMoneyForm = false;
	}
	
	
	//temp
	$scope.addCustomEvent = function(typeAndText) {
		AccountService.addCustomEvent(typeAndText, typeAndText);
	}
	
	//Delete an event from the event list
	//Parameter: the entire event object you'd like to remove
	$scope.deleteEvent = function(index) {
		AccountService.deleteEvent(index);
	}

})