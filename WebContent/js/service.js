angular.module('pocketMoneyApp').factory('AccountService', function($firebaseObject, $firebaseArray) {

	//Base Firebase URL
	var BASE_URL = "https://moneygotchi.firebaseio.com/users/";
	var accountID = -1;
	
	//The main object returned by the factory
	var accountInformation = {
			login: init,
			balanceEurocents: 0,
			events: [],
			addCustomEvent: addCustomEvent,
			deleteEvent: deleteEvent,
			giveMoney: giveMoney
	};
	
	//Initialise the accountInformation object with data from the account
	//private
	function init(id) {
		accountID = id;
		accountInformation.balanceEurocents = $firebaseObject(buildEndPoint("amount"));
		accountInformation.events = $firebaseArray(buildEndPoint("events"));
		accountInformation.events;
		console.log(accountInformation.events);
	}
	
	
	//Give the child some money
	//Inserts the new event
	//parameter: amount (the amount to give)
	function giveMoney(amount) {
		//calculate new balance: 
		centAmount = amount*100;		
		var newBalance = centAmount + accountInformation.balanceEurocents.$value;

		//save balance:
		//accountInformation.balanceEurocents.$value = newBalance;
		//accountInformation.balanceEurocents.$save();
		
		//create event:
		var timestamp = new Date().getTime();
		var eventObject = {
				type: "newmoney",
				text: "Nieuw zakgeld! " + amount + " euro.",
				amount: centAmount,
				time: timestamp
		};
		addEvent(eventObject)
	}
	
	
	//random function to create a hello world event
	function addEventHello() {
		var timestamp = new Date().getTime();
		var eventObject = {
				type: "dummy",
				text: "Hello World",
				time: timestamp
		};
		addEvent(eventObject);
	}
	
	//Add an event with a custom name and description
	//Temp?
	function addCustomEvent(type, text) {
		var timestamp = new Date().getTime();
		var eventObject = {
				type: type,
				text: text,
				time: timestamp
		};
		addEvent(eventObject);
	}
	
	//Add an event to the list
	//private
	function addEvent(eventObject) {
		var events = $firebaseArray(buildEndPoint("events"));
		events.$add(eventObject).then(function(ref) {
			//Store id as attribute of new event
			var newEventId = $firebaseObject(buildEndPoint("events/" + ref.key() + "/id"));
			newEventId.$value = ref.key();
			newEventId.$save();
		});
	}
	
	//Delete an event from the event list
	//Parameter: the entire event object you'd like to remove
	function deleteEvent(index) {
		accountInformation.events.$remove(index);
	}
	

	//Build FireBase endpoint
	function buildEndPoint(key) {
		return new Firebase(BASE_URL + accountID + '/' + key);
	}

	return accountInformation;
});