package com.pocketmoney.moneygotchi.child

import android.util.Log
import com.pocketmoney.moneygotchi.command.CommandState
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*

/**
 * Created by Thijs Eckhart on 15-3-2018.
 */
data class Child(val id: String = "", val name: String = "Unknown Child", var money: Int = 0, var maxMoney: Int = 10000) {
    fun spendMoney(i: Int): CommandState {
        Log.i(Child::class.java.simpleName, "spendMoney: $i  and $money")
        return if (i <= money) {
            money -= i
            CommandState.SUCCESS
        } else {
            CommandState.NO_RESOURCES
        }
    }

}


val euroFormat = NumberFormat.getCurrencyInstance(Locale("nl", "NL"))

fun BigDecimal.toEuroString() = euroFormat.format(this)

fun Int.fromCentToEuro(): BigDecimal = BigDecimal(this).divide(BigDecimal(100))