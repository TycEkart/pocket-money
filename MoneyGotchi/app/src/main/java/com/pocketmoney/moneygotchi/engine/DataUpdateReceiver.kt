package com.pocketmoney.moneygotchi.engine

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class DataUpdateReceiver(val updater: () -> Unit) : BroadcastReceiver() {
    companion object {
        val REFRESH_DATA_INTENT = "timeToUpdateIntent"
    }

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == REFRESH_DATA_INTENT) {
            updater()
        }
    }
}