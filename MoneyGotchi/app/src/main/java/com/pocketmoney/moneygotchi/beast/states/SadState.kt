package com.pocketmoney.moneygotchi.beast.states

import com.pocketmoney.moneygotchi.beast.Beast
import com.pocketmoney.moneygotchi.beast.BeastState
import com.pocketmoney.moneygotchi.beast.BeastStatePattern
import com.pocketmoney.moneygotchi.child.Child

/**
 * Created by tyc on 16/03/2018.
 */
class SadState : BeastStatePattern {
    override val beastState: BeastState = BeastState.SAD

    private val thresholdMoment = 1000L
    private var reduceMoment = 0L


    override fun update(current: Long, delta: Long, child: Child, beast: Beast, feedback: (BeastStatePattern) -> Unit) {
        reduceMoment += delta
        if (reduceMoment > thresholdMoment) {
            reduceMoment = 0
            beast.reduceHappiness(1)
            feedback(if (beast.happyLevel == 0) {
                beast.happyLevel = 10
                NormalState()
            } else {
                this
            })
        }
    }

}