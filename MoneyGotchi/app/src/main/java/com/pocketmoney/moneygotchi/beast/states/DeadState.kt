package com.pocketmoney.moneygotchi.beast.states

import com.pocketmoney.moneygotchi.beast.Beast
import com.pocketmoney.moneygotchi.beast.BeastState
import com.pocketmoney.moneygotchi.beast.BeastStatePattern
import com.pocketmoney.moneygotchi.child.Child

/**
 * Created by tyc on 16/03/2018.
 */
class DeadState : BeastStatePattern {
    override val beastState: BeastState = BeastState.DEAD

    companion object {
        val happinessThreshold = 7
    }

    override fun update(current: Long, delta: Long, child: Child, beast: Beast, feedback: (BeastStatePattern) -> Unit) {
        feedback(this)
    }

}