package com.pocketmoney.moneygotchi.fragments

import android.app.Fragment
import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pocketmoney.moneygotchi.R
import com.pocketmoney.moneygotchi.beast.Beast
import com.pocketmoney.moneygotchi.beast.BeastStatePattern
import com.pocketmoney.moneygotchi.beast.states.HappyState
import com.pocketmoney.moneygotchi.beast.states.NormalState
import com.pocketmoney.moneygotchi.beast.states.SleepState
import com.pocketmoney.moneygotchi.beast.states.SleepState.Companion.SLEEP_TIME
import kotlinx.android.synthetic.main.fragment_beast.view.*

/**
 * Created by Thijs Eckhart on 15-3-2018.
 */
class BeastFragment : Fragment() {
    lateinit var layout: View
    lateinit var beast: Beast
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        layout = inflater.inflate(R.layout.fragment_beast, container, false)
        return layout
    }

    fun updateBeast(beast: Beast) {
        this.beast = beast
    }

    fun update(currentState: BeastStatePattern) {
        layout.beastImage.setImageResource(currentState.beastState.beastImageResource)

        when (currentState) {
            is SleepState -> {
                var remainder = currentState.remainderTime()
                if (remainder < 0) {
                    remainder = 0
                }
                layout.debug.text = "remainder: $remainder"

                layout.progress.also {
                    it.visibility = View.VISIBLE
                    it.setProgressWithAnimation(remainder.toFloat() / SLEEP_TIME * 100, 100)
                }
            }
            is HappyState -> {
                if (!currentState.hasPlayed) {
                    currentState.hasPlayed = true
                    MediaPlayer.create(activity.baseContext, R.raw.oinks_15_sec).start()
                }
            }
            else -> {
                layout.progress.visibility = View.GONE
                layout.debug.text = """
                Energy     = ${beast.energyLevel}
                Happiness  = ${beast.happyLevel}
                Fedness    = ${beast.fedLevel}
                """.trimIndent()
            }
        }
    }


}