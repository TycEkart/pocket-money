package com.pocketmoney.moneygotchi.fragments

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pocketmoney.moneygotchi.R
import kotlinx.android.synthetic.main.fragment_vault.view.*
import android.support.v4.view.accessibility.AccessibilityRecordCompat.setSource
import android.media.MediaPlayer
import org.jetbrains.anko.act


/**
 * Created by Thijs Eckhart on 27-3-2018.
 */
class VaultFragment : Fragment() {
    lateinit var layout: View

    companion object {
        var open = false
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        layout = inflater.inflate(R.layout.fragment_vault, container, false)

        updateVaultImage()
        layout.vault.setOnClickListener {
            open = !open
            if (open)
                MediaPlayer.create(activity.baseContext, R.raw.creeking_door).start()
            updateVaultImage()
        }

        return layout
    }

    private fun updateVaultImage() {
        layout.vault.setImageResource(if (open) R.drawable.vault_open else R.drawable.vault_closed)
    }

}