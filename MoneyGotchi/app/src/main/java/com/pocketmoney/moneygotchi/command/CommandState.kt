package com.pocketmoney.moneygotchi.command

enum class CommandState {
    SUCCESS,
    NO_RESOURCES,
    NOT_AVAILABLE
}