package com.pocketmoney.moneygotchi.beast.states

import com.pocketmoney.moneygotchi.beast.Beast
import com.pocketmoney.moneygotchi.beast.BeastState
import com.pocketmoney.moneygotchi.beast.BeastStatePattern
import com.pocketmoney.moneygotchi.child.Child

/**
 * Created by tyc on 16/03/2018.
 */
class SleepState : BeastStatePattern {

    companion object {
        val SLEEP_TIME = 3000L
        val SLEEP_VALUE_PER_MILI_SECOND = 0.001
    }

    override val beastState: BeastState = BeastState.SLEEPING
    private var sleepRewarded = 0
    private var onStartSleep: Long = 0L
    private var timeLeft = SLEEP_TIME


    override fun update(current: Long, delta: Long, child: Child, beast: Beast, feedback: (BeastStatePattern) -> Unit) {
        if (onStartSleep == 0L) {
            timeLeft = SLEEP_TIME
            sleepRewarded = 0
            onStartSleep = System.currentTimeMillis()
        }
        val newSleepRewarded = (SLEEP_VALUE_PER_MILI_SECOND * (current - onStartSleep)).toInt()
        val deltaSleepRewarded = newSleepRewarded - sleepRewarded
        if (deltaSleepRewarded > 0) {
            sleepRewarded+=deltaSleepRewarded
            beast.energyLevel += deltaSleepRewarded
        }
        timeLeft -= delta
        feedback(this)
        if (onStartSleep + SLEEP_TIME <= current) {
            feedback(NormalState())
        }
    }

    fun remainderTime(): Long {
        return timeLeft
    }
}