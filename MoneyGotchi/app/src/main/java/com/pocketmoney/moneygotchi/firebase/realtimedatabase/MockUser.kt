package com.pocketmoney.moneygotchi.firebase.realtimedatabase

/**
 * Created by Thijs Eckhart on 28-3-2018.
 */
data class MockUser(var balanceEuro: Int = 0,
               val cleanliness: Int = 0,
               val happiness: Int = 0,
               val health: Int = 0,
               val hungriness: Int = 0,
               val events: HashMap<String, Event> = hashMapOf())

data class Event(val amount: Int = 0,
            val text: String = "-",
            val time: Long = 0L,
            val type: String = "undefined Type")
