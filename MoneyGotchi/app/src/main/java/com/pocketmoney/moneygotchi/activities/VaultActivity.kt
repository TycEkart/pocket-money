package com.pocketmoney.moneygotchi.activities

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.pocketmoney.moneygotchi.R
import com.pocketmoney.moneygotchi.coins.Coin
import com.pocketmoney.moneygotchi.coins.CoinAdapter
import kotlinx.android.synthetic.main.activity_vault.view.*
import org.jetbrains.anko.contentView
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import com.pocketmoney.moneygotchi.child.fromCentToEuro
import com.pocketmoney.moneygotchi.child.toEuroString
import kotlinx.android.synthetic.main.activity_vault.*
import android.view.ViewTreeObserver
import android.widget.Toast
import com.pocketmoney.moneygotchi.firebase.realtimedatabase.DatabaseHandler
import com.pocketmoney.moneygotchi.firebase.realtimedatabase.DatabaseHandler.registerAndRetrieve
import com.pocketmoney.moneygotchi.firebase.realtimedatabase.Event
import com.pocketmoney.moneygotchi.firebase.realtimedatabase.MockUser
import com.pocketmoney.moneygotchi.fragments.VaultFragment
import kotlinx.android.synthetic.main.fragment_vault.view.*
import java.util.ArrayList


class VaultActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vault)

        val contentView = contentView!!

        contentView.piggy.setOnClickListener {
            val intent = Intent(applicationContext, GameActivity::class.java)
            startActivity(intent)
        }
        contentView.recyclerView.also {
            val mGridLayoutManager = GridLayoutManager(this, spanCount)
            it.adapter = adapter
            recyclerView.viewTreeObserver.addOnGlobalLayoutListener(viewTreeObserver)
            it.layoutManager = mGridLayoutManager
        }
        adapter.notifyDataSetChanged()
    }

    val spanCount = 10
    val viewTreeObserver: ViewTreeObserver.OnGlobalLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
        adapter.setListWidth(recyclerView.measuredWidth, spanCount)
        removeObserver()
    }

    private fun removeObserver() {
        recyclerView.viewTreeObserver.removeOnGlobalLayoutListener(viewTreeObserver)
    }

    var mockUser: MockUser = MockUser()


    private val collected: (Coin, (Boolean) -> Unit) -> Unit = { coin, allowed ->
        val isOpen = VaultFragment.open
        allowed(isOpen)
        if (isOpen) {
            mockUser.balanceEuro += coin.cents
            DatabaseHandler.updateMockUser(mockUser)
            MediaPlayer.create(baseContext, R.raw.chaching).start()
        } else {
            Toast.makeText(baseContext, "Kluis is dicht!", Toast.LENGTH_SHORT).show()
        }
    }
    val adapter = CoinAdapter(collected)

    override fun onStart() {
        super.onStart()
        registerAndRetrieve { mockUser ->
            Log.i("VaultActivity", "onStart: retrieve once handle MU:$mockUser")
            if (mockUser != null) {
                contentView!!.saldo.text = mockUser.balanceEuro.fromCentToEuro().toEuroString()
                var easternEgg: Event? = null
                this.mockUser = mockUser
                mockUser.events.forEach {
                    val event = it.value
                    when (event.type) {
                        "newmoney" -> {
                            Toast.makeText(this, event.text, Toast.LENGTH_SHORT).show()
                            addCoins(event.amount)
                            mockUser.events[it.key] = event.copy(type = "old")
                        }
                        "crash" -> {
                            easternEgg = event
                            mockUser.events[it.key] = event.copy(type = "crash!")
                        }
                        else -> {
                            //ignore
                        }
                    }
                }
                DatabaseHandler.updateMockUser(mockUser)
                easternEgg.also {
                    if (it != null) {
                        MediaPlayer.create(baseContext, R.raw.doh).start()
                        //Toast.makeText(baseContext, it.text, Toast.LENGTH_LONG).show()
                        throw Exception("DOH!")
                    }
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        DatabaseHandler.unregisterall()
    }

    private fun addCoins(amount: Int) {
        var rest = amount
//500

        val c200 = rest / 200; rest %= 200
        val c100 = rest / 100; rest %= 100
        val c050 = rest / 50; rest %= 50
        val c020 = rest / 20; rest %= 20
        val c010 = rest / 10; rest %= 10
        val c005 = rest / 5; rest %= 5
        adapter.list.also {
            it.addAll(getCoins(Coin.coin_200, c200))
            it.addAll(getCoins(Coin.coin_100, c100))
            it.addAll(getCoins(Coin.coin_050, c050))
            it.addAll(getCoins(Coin.coin_020, c020))
            it.addAll(getCoins(Coin.coin_010, c010))
            it.addAll(getCoins(Coin.coin_005, c005))
        }



        adapter.notifyDataSetChanged()
    }

    fun calcCooins() {

    }

    fun getCoins(typeCoin: Coin, number: Int): ArrayList<Coin> {
        val list = ArrayList<Coin>()
        for (i in 1..number) {
            list.add(typeCoin)
        }
        return list
    }

}