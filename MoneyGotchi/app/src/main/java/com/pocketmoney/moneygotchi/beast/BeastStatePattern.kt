package com.pocketmoney.moneygotchi.beast

import com.pocketmoney.moneygotchi.child.Child

/**
 * Created by tyc on 16/03/2018.
 */
interface BeastStatePattern {
    fun update(current: Long, delta: Long, child: Child, beast: Beast, feedback: (BeastStatePattern) -> Unit)
    val beastState: BeastState

}