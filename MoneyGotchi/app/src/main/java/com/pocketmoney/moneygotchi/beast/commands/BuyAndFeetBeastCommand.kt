package com.pocketmoney.moneygotchi.beast.commands

import com.pocketmoney.moneygotchi.beast.Beast
import com.pocketmoney.moneygotchi.beast.BeastState
import com.pocketmoney.moneygotchi.command.Command
import com.pocketmoney.moneygotchi.command.CommandState
import com.pocketmoney.moneygotchi.engine.EngineService

/**
 * Created by tyc on 15/03/2018.
 */
class BuyAndFeetBeastCommand(val beast: Beast) : Command {
    override fun execute(completed: (CommandState) -> Unit) {
        val state = EngineService.currentState.beastState
        when {
            state != BeastState.SLEEPING -> {


                val spendState = beast.ownerChild.spendMoney(500)
                when (spendState) {
                    CommandState.SUCCESS -> {
                        beast.increaseFed(5)
                        completed(CommandState.SUCCESS)
                    }
                    else -> completed(spendState)
                }
            }
            else -> completed(CommandState.NOT_AVAILABLE)
        }
    }

    override fun undo(): Command? {
        return null
    }
}