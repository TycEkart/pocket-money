package com.pocketmoney.moneygotchi.command

/**
 * Created by tyc on 15/03/2018.
 */
interface Command {
    fun execute(completed: (CommandState) -> Unit)
    fun undo(): Command?
}