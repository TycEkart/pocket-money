package com.pocketmoney.moneygotchi.coins

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pocketmoney.moneygotchi.R
import kotlinx.android.synthetic.main.list_item_coin.view.*


/**
 * Created by Thijs Eckhart on 27-3-2018.
 */
class Coin(val centImageResource: Int, val cents: Int) {


    companion object {
        val coin_005 = Coin(R.drawable.cent_005, 5)
        val coin_010 = Coin(R.drawable.cent_010, 10)
        val coin_020 = Coin(R.drawable.cent_020, 20)
        val coin_050 = Coin(R.drawable.cent_050, 50)
        val coin_100 = Coin(R.drawable.cent_100, 100)
        val coin_200 = Coin(R.drawable.cent_200, 200)
    }
}


class CoinHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun onBind(coin: Coin, itemLayoutWidth: Int) {
        itemView.coin.setImageResource(coin.centImageResource)
        val params = itemView.layoutParams
        params.width = itemLayoutWidth
        params.height = itemLayoutWidth
        //itemView.layoutParams = params
    }
}

class CoinAdapter(val onCollected: (Coin, (Boolean) -> Unit) -> Unit) : RecyclerView.Adapter<CoinHolder>() {
    var itemLayoutWidth = 49

    private val TAG = "CoinAdapter"

    fun setListWidth(width: Int, spanCount: Int) {
        itemLayoutWidth = width / spanCount
        notifyDataSetChanged()
    }

    val list = ArrayList<Coin>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoinHolder {
        val inflater = parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.list_item_coin, parent, false)
        return CoinHolder(view)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CoinHolder, position: Int) {
        holder.onBind(list[position], itemLayoutWidth)

        holder.itemView.coin.setOnClickListener {
            onCollected(list[position]) { allowed ->
                if (allowed) {
                    list.removeAt(position)
                    notifyDataSetChanged()
                }
            }
        }
    }


}