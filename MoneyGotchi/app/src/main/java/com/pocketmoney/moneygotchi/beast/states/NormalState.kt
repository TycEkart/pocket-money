package com.pocketmoney.moneygotchi.beast.states

import com.pocketmoney.moneygotchi.beast.Beast
import com.pocketmoney.moneygotchi.beast.BeastState
import com.pocketmoney.moneygotchi.beast.BeastStatePattern
import com.pocketmoney.moneygotchi.child.Child
import java.util.*

/**
 * Created by tyc on 16/03/2018.
 */
class NormalState : BeastStatePattern {
    val random = Random()

    fun rand(from: Int, to: Int): Int {
        return random.nextInt(to - from) + from
    }
    override val beastState: BeastState = BeastState.NORMAL

    private val REDUCE_MOEMENT_THRESHHOLD = 1000L
    private var reduceMoment = 0L


    override fun update(current: Long, delta: Long, child: Child, beast: Beast, feedback: (BeastStatePattern) -> Unit) {
        reduceMoment += delta
        if (reduceMoment > REDUCE_MOEMENT_THRESHHOLD) {
            reduceMoment = 0
            when (rand(0, 3)) {
                0 -> beast.reduceFed(1)
                1 -> beast.reduceHappiness(1)
                2 -> beast.reduceEnergy(1)
            }
        }
        feedback(calculateState(beast))
    }

    private fun calculateState(beast: Beast): BeastStatePattern {
        return when {
            beast.energyLevel == 0 -> SleepState()
            beast.fedLevel <= 3 -> HungryState()
            beast.happyLevel <= 3 -> SadState()
            beast.happyLevel >= HappyState.happinessThreshold -> HappyState()

            else -> this
        }
    }

}