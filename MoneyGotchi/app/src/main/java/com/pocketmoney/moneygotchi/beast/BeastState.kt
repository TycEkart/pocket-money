package com.pocketmoney.moneygotchi.beast

import com.pocketmoney.moneygotchi.R

/**
 * Created by Thijs Eckhart on 15-3-2018.
 */
enum class BeastState(val beastImageResource: Int) {
    NORMAL(R.drawable.ic_piggy_temp_normal),
    HAPPY(R.drawable.ic_piggy_happy), SAD(R.drawable.ic_piggy_sad),
    HUNGRY(R.drawable.ic_piggy_hungry), FED(R.drawable.ic_piggy_feeded),
    SLEEPING(R.drawable.ic_piggy_sleeping), ENERGIZED(R.drawable.ic_piggy_energyzed),
    DEAD(R.drawable.happy_dead_pig);

    lateinit var opposite: BeastState

    companion object {
        init {
            HAPPY.opposite = SAD
            HUNGRY.opposite = FED
            SLEEPING.opposite = ENERGIZED

            SAD.opposite = HAPPY
            FED.opposite = HUNGRY
            ENERGIZED.opposite = SLEEPING
        }
    }

}