package com.pocketmoney.moneygotchi.activities

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.pocketmoney.moneygotchi.R
import com.pocketmoney.moneygotchi.beast.Beast
import com.pocketmoney.moneygotchi.beast.commands.BuyAndFeetBeastCommand
import com.pocketmoney.moneygotchi.beast.commands.SleepBeastCommand
import com.pocketmoney.moneygotchi.child.Child
import com.pocketmoney.moneygotchi.child.fromCentToEuro
import com.pocketmoney.moneygotchi.child.toEuroString
import com.pocketmoney.moneygotchi.command.CommandState
import com.pocketmoney.moneygotchi.engine.DataUpdateReceiver
import com.pocketmoney.moneygotchi.engine.EngineService
import com.pocketmoney.moneygotchi.engine.EngineService.Companion.beast
import com.pocketmoney.moneygotchi.engine.EngineService.Companion.child
import com.pocketmoney.moneygotchi.fragments.BeastFragment
import kotlinx.android.synthetic.main.activity_game.view.*
import org.jetbrains.anko.contentView


class GameActivity : AppCompatActivity() {

    private val beastFragment = BeastFragment()
    lateinit var progressbar: ProgressBar
    lateinit var childMoney: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        fragmentManager.beginTransaction().add(R.id.holder_fragment1, beastFragment).commit()
        val view = contentView!!
        childMoney = view.childMoney
        progressbar = view.progressbar

        view.button_vault.setOnClickListener {
            val intent = Intent(baseContext, VaultActivity::class.java)
            startActivity(intent)
        }

        view.button_1.setOnClickListener {
            SleepBeastCommand(beast).execute {
                update()
            }
        }
        view.button_2.setOnClickListener {
            beast.reset()

            update()
        }
        view.button_3.setOnClickListener {
            BuyAndFeetBeastCommand(beast).execute {
                if (it != CommandState.SUCCESS)
                    Toast.makeText(applicationContext, "FAILED: " + it, Toast.LENGTH_SHORT).show()
                update()
            }
        }
        view.button_4.setOnClickListener {
            Toast.makeText(applicationContext, "RECEIVED MONEY: 20", Toast.LENGTH_SHORT).show()
            child.money += 2000
            update()
        }

    }

    override fun onStart() {
        super.onStart()
        update()
        val intent = Intent(this, EngineService::class.java)
        startService(intent)
    }

    private fun update() {
        setChild(child)
        setBeast(beast)
    }

    private fun setChild(child: Child) {
        progressbar.max = child.maxMoney
        progressbar.progress = child.money
        childMoney.text = child.money.fromCentToEuro().toEuroString()

    }

    fun setBeast(beast: Beast) {
        beastFragment.updateBeast(beast)
    }

    private var dataUpdateReceiver: DataUpdateReceiver? = null

    override fun onResume() {
        super.onResume()
        if (dataUpdateReceiver == null) dataUpdateReceiver = DataUpdateReceiver(updater)
        val intentFilter = IntentFilter(DataUpdateReceiver.REFRESH_DATA_INTENT)
        registerReceiver(dataUpdateReceiver, intentFilter)
    }


    val updater: () -> Unit = {
        update()
        beastFragment.update(EngineService.currentState)
    }

    override fun onPause() {
        super.onPause()
        if (dataUpdateReceiver != null) unregisterReceiver(dataUpdateReceiver)
    }
}