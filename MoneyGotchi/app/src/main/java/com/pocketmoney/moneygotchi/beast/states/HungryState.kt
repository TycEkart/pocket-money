package com.pocketmoney.moneygotchi.beast.states

import com.pocketmoney.moneygotchi.beast.Beast
import com.pocketmoney.moneygotchi.beast.BeastState
import com.pocketmoney.moneygotchi.beast.BeastStatePattern
import com.pocketmoney.moneygotchi.child.Child
import com.pocketmoney.moneygotchi.engine.EngineService

/**
 * Created by tyc on 16/03/2018.
 */
class HungryState : BeastStatePattern {
    override val beastState: BeastState = BeastState.HUNGRY

    private val REDUCE_MOEMENT_THRESHHOLD = 1000L
    private var reduceMoment = 0L


    override fun update(current: Long, delta: Long, child: Child, beast: Beast, feedback: (BeastStatePattern) -> Unit) {
        reduceMoment += delta
        if (reduceMoment > REDUCE_MOEMENT_THRESHHOLD) {
            reduceMoment = 0
            beast.reduceFed(1)
            var state: BeastStatePattern = this
            when {
                beast.fedLevel == 0 -> EngineService.child.money -= 100
                beast.fedLevel > 3 -> state = NormalState()
                else -> {
                }
            }
            if (EngineService.child.money <= 0) {
                state = DeadState()
            }
            feedback(state)

        }
    }

}