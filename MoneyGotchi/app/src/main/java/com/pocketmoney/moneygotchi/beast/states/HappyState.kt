package com.pocketmoney.moneygotchi.beast.states

import android.media.MediaPlayer
import com.pocketmoney.moneygotchi.R
import com.pocketmoney.moneygotchi.beast.Beast
import com.pocketmoney.moneygotchi.beast.BeastState
import com.pocketmoney.moneygotchi.beast.BeastStatePattern
import com.pocketmoney.moneygotchi.child.Child

/**
 * Created by tyc on 16/03/2018.
 */
class HappyState : BeastStatePattern {
    override val beastState: BeastState = BeastState.HAPPY
    var hasPlayed = false

    private val thresholdMoment = 10000L
    private var reduceMoment = 0L
    private var onStartState: Long = 0L

    companion object {
        val happinessThreshold = 7
    }

    override fun update(current: Long, delta: Long, child: Child, beast: Beast, feedback: (BeastStatePattern) -> Unit) {
        if (onStartState == 0L) {
            hasPlayed = false
            onStartState = current
        }
        reduceMoment += delta

        if (reduceMoment > thresholdMoment) {
            reduceMoment = 0

            beast.happyLevel = happinessThreshold - 1

            feedback(NormalState())
        }
    }

}