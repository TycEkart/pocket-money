package com.pocketmoney.moneygotchi.engine

import android.app.Service
import android.content.Intent
import android.os.*
import android.os.Process.THREAD_PRIORITY_BACKGROUND
import android.util.Log
import android.widget.Toast
import com.pocketmoney.moneygotchi.beast.Beast
import com.pocketmoney.moneygotchi.beast.BeastStatePattern
import com.pocketmoney.moneygotchi.beast.states.NormalState
import com.pocketmoney.moneygotchi.beast.states.SleepState
import com.pocketmoney.moneygotchi.child.Child


/**
 * Created by tyc on 15/03/2018.
 */
class EngineService : Service() {
    private var mServiceLooper: Looper? = null
    private var mServiceHandler: ServiceHandler? = null

    // Handler that receives messages from the thread
    private inner class ServiceHandler(looper: Looper) : Handler(looper) {
        override fun handleMessage(msg: Message) {
            try {
                while (true) {
                    Thread.sleep(TIME_REFRESH_RATE)
                    update()
                }
            } catch (e: InterruptedException) {
                // Restore interrupt status.
                Thread.currentThread().interrupt()
            }
            // Stop the service using the startId, so that we don't stop
            // the service in the middle of handling another job
            stopSelf(msg.arg1)
        }
    }

    override fun onCreate() {
        Log.i(EngineService::class.java.simpleName, "onCreate():")
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        val thread = HandlerThread("ServiceStartArguments", THREAD_PRIORITY_BACKGROUND)
        thread.start()
        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper?.quitSafely()
        mServiceLooper = thread.looper.also {
            mServiceHandler = ServiceHandler(it)
        }

    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.i(EngineService::class.java.simpleName, "onStartCommand():")
        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        mServiceHandler?.also {
            val msg = it.obtainMessage()
            msg.arg1 = startId
            it.sendMessage(msg)
        }
        // If we get killed, after returning from here, restart
        return Service.START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        // We don't provide binding, so return null
        return null
    }

    override fun onDestroy() {
        Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show()
    }

    private var lastUpdateTime: Long = System.currentTimeMillis()

    private fun update() {
        val currentTime = System.currentTimeMillis()
        update(currentTime, currentTime - lastUpdateTime)
        lastUpdateTime = currentTime
    }

    companion object {
        private val TIME_REFRESH_RATE = 100L
        val child = Child("", "Kud Kind", 2000, 10000)
        val beast = Beast(child)
        var currentState: BeastStatePattern = NormalState()
    }

    private fun update(current: Long, delta: Long) {
        currentState.update(current, delta, child, beast) {
            if (currentState != it) currentState = it
            sendBroadcast(Intent(DataUpdateReceiver.REFRESH_DATA_INTENT))
        }
    }



}


