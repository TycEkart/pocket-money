package com.pocketmoney.moneygotchi.firebase.realtimedatabase

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.coroutines.experimental.asReference

/**
 * Created by Thijs Eckhart on 28-3-2018.
 */
object DatabaseHandler : ValueEventListener {
    val TAG = "DBH"

    val userRef = FirebaseDatabase.getInstance().getReference("mockuser")

    var listener: ((MockUser?) -> Unit)? = null

    fun registerAndRetrieve(listener: (MockUser?) -> Unit) {
        if (this.listener == null) {
            this.listener = listener
            userRef.addValueEventListener(this)
        } else {
            this.listener = listener
        }
    }


    override fun onDataChange(mockuserdata: DataSnapshot?) {
        if (mockuserdata != null) {
            val mu = mockuserdata.getValue(MockUser::class.java)
            listener?.invoke(mu)
        } else {
            listener?.invoke(null)
        }
    }

    override fun onCancelled(p0: DatabaseError?) {
        listener?.invoke(null)
    }


    fun updateMockUser(mockUser: MockUser) {
        userRef.setValue(mockUser)
    }

    fun unregisterall() {
        listener = null
        userRef.removeEventListener(this)
    }

}
