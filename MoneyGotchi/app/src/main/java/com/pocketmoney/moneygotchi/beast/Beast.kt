package com.pocketmoney.moneygotchi.beast

import com.pocketmoney.moneygotchi.beast.states.NormalState
import com.pocketmoney.moneygotchi.child.Child
import com.pocketmoney.moneygotchi.engine.EngineService

/**
 * Created by Thijs Eckhart on 15-3-2018.
 */
data class      Beast(val ownerChild: Child,
                 var fedLevel: Int = 5,
                 var energyLevel: Int = 5,
                 var happyLevel: Int = 5) {
    val MAX_LEVEL = 10
    val MIN_LEVEL = 0


    fun reduceFed(i: Int) {
        fedLevel -= i
        if (fedLevel < MIN_LEVEL) {
            fedLevel = MIN_LEVEL
        }
    }

    fun reduceHappiness(i: Int) {
        happyLevel -= i
        if (happyLevel < MIN_LEVEL) {
            happyLevel = MIN_LEVEL
        }
    }

    fun reduceEnergy(i: Int) {
        energyLevel -= i
        if (energyLevel < MIN_LEVEL) {
            energyLevel = MIN_LEVEL
        }
    }

    fun reset() {
        energyLevel = 6
        happyLevel = 6
        fedLevel = 6
        EngineService.currentState = NormalState()
    }

    fun increaseFed(i: Int) {
        fedLevel += i
        if (fedLevel > MAX_LEVEL) {
            fedLevel = MAX_LEVEL
        }
    }


}

