package com.pocketmoney.moneygotchi.beast.commands

import com.pocketmoney.moneygotchi.beast.Beast
import com.pocketmoney.moneygotchi.beast.states.SleepState
import com.pocketmoney.moneygotchi.command.Command
import com.pocketmoney.moneygotchi.command.CommandState
import com.pocketmoney.moneygotchi.engine.EngineService

/**
 * Created by tyc on 15/03/2018.
 */
class SleepBeastCommand(val beast: Beast) : Command {
    override fun execute(completed: (CommandState) -> Unit) {
       EngineService.currentState = SleepState()
    }

    override fun undo(): Command? {
        return null
    }
}