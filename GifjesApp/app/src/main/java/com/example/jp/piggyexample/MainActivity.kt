package com.example.jp.piggyexample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import java.util.*


class MainActivity : AppCompatActivity() {

    var toggle = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun makeHappy(v: View) {
        val im = findViewById(R.id.piggyImage) as pl.droidsonroids.gif.GifTextView

        //Change image to 'happy' or 'jump':
        if (toggle)
            im.setBackgroundResource(R.drawable.happy1)
        else
            im.setBackgroundResource(R.drawable.jump1)
        toggle = !toggle

        //After 1 second, change it back
        Timer().schedule(object : TimerTask() {
            override fun run() {
                im.setBackgroundResource(R.drawable.idle_loop)
            }
        }, 1000)
    }
}
